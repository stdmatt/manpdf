# manpdf

**Made with <3 by [stdmatt](http://stdmatt.com).**

## Description 
```manpdf``` exports the __man pages__ and __pydocs__ in nicely formatted
pdf files, letting you to do whatever your pdf reader/editor is capable of.

This means that with it we can create annotations in the documentation pages
for the parts that are more interesting and providing a better reading
experience.

It also has the ability of setting a git repository as the output folder
enable us to sync the generated files between several machines.

Lastly it saves the documentation for each platform independently, so the GNU/Linux,
OSX, etc. pages don't get mixed with the other ones.

<br>

As usual, you are **very welcomed** to **share** and **hack** it.


## Examples:

### Saving and reading a man page:
<center>
    <img src="res/manpdf_usage.gif">
</center>

### Syncing using a git repository:
<center>
    <img  src="res/manpdf_sync.gif">
</center>


## Usage

```shell script
Usage:
  manpdf [--help] [--version] [--sync] [--show-config]
  manpdf [--setup] [--setup <output-path> <git-url>]
  manpdf [section] page

Options:
    * --help                          - Show this screen.
    * --version                       - Show copyright and version information.
    * --sync                          - Synchronize the saved manual pages.
    * --setup                         - Setup the program settings - Interactive mode.
    * --setup <output-path> <git-url> - Setup the program settings.
    * --show-config                   - Shows the current configuration.

Notes:
  Options marked with * are exclusive i.e. they'll run and exit without
  considering the other options.e
```

## WSL 

On [WSL](https://en.wikipedia.org/wiki/Windows_Subsystem_for_Linux) the environment variable ```MANPDF_READER``` with 
the Windows Path - it'll be converted by the ```/bin/wslpath```a - for the PDF reader should be exported. 

Usually it would go into the ```.basrc``` like:
```shell script
export MANPDF_READER="C://path/to/the/pdf/reader.exe";
```


## Dependencies:

```mandpdf``` depends upon the following libraries, but doesn't install any of the by itself.

- [shellscript_utils](https://gitlab.com/stdmatt-libs/shellscript_utils.git)
- [ps2pdf](https://web.mit.edu/ghostscript/www/Ps2pdf.htm)


## Install:

```shell script
## 1- Clone...
$ git clone https://gitlab.com/stdmatt-tools/manpdf.git
## 2 - Go to directory...
$ cd ./mandpf.git
## 3 - Install...
$ sudo ./install.sh 
## 4 If you want to sync between machines...
$ manpdf --setup
## 4 - Have fun ;D
```

## License:

This software is released under [GPLv3](https://www.gnu.org/licenses/gpl-3.0.en.html).


## Others:

There's more FLOSS things at [stdmatt.com](https://stdmatt.com) :)
