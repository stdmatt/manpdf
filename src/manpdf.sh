#! /usr/bin/env bash
##~---------------------------------------------------------------------------##
##                        _      _                 _   _                      ##
##                    ___| |_ __| |_ __ ___   __ _| |_| |_                    ##
##                   / __| __/ _` | '_ ` _ \ / _` | __| __|                   ##
##                   \__ \ || (_| | | | | | | (_| | |_| |_                    ##
##                   |___/\__\__,_|_| |_| |_|\__,_|\__|\__|                   ##
##                                                                            ##
##  File      : manual.sh                                                     ##
##  Project   : manpdf                                                        ##
##  Date      : Feb 25, 2017                                                  ##
##  License   : GPLv3                                                         ##
##  Author    : stdmatt <stdmatt@pixelwizards.io>                             ##
##  Copyright : stdmatt - 2017, 2020                                          ##
##                                                                            ##
##  Description :                                                             ##
##    Functions to save/export manual pages pdf format.                       ##
##---------------------------------------------------------------------------~##

##----------------------------------------------------------------------------##
## Imports                                                                    ##
##----------------------------------------------------------------------------##
source /usr/local/src/stdmatt/shellscript_utils/main.sh


##----------------------------------------------------------------------------##
## Constants                                                                  ##
##----------------------------------------------------------------------------##
PROGRAM_NAME="manpdf";
PROGRAM_VERSION="v1.5.0";
COPYRIGHT_YEARS="2017 - 2020"

USER_HOME="$(pw_get_user_home)";
RC_PATH="$USER_HOME/.stdmatt/manpdf_rc"

DEFAULT_OUTPUT_PATH="$USER_HOME/Documents/manpdf-pages";


##----------------------------------------------------------------------------##
## Helper Functions                                                           ##
##----------------------------------------------------------------------------##
##------------------------------------------------------------------------------
_manpdf_get_option_from_file()
{
    local filename="$1";
    local option_name="$2";

    if [ -e "$filename" ]; then
        local contents=$(cat "$filename" | grep "$option_name" | cut -d"=" -f2);
        echo "$contents";
    else
        echo "";
    fi;
}

##------------------------------------------------------------------------------
_manpdf_get_output_path()
{
    local option_contents=$(_manpdf_get_option_from_file "$RC_PATH" "output-path");
    local output_path=$(pw_expand_user_and_make_abs $(pw_trim "$option_contents"));

    echo "$output_path";
}

##------------------------------------------------------------------------------
_manpdf_get_pages_repo_url()
{
    local option_contents=$(_manpdf_get_option_from_file "$RC_PATH" "gitrepo");
    local repo_url=$(pw_trim "$option_contents");

    echo "$repo_url";
}

##------------------------------------------------------------------------------
## @todo(stdmatt): Currently working just for OSX...
##     Make a way to get the fullpath for the target link....
_manpdf_get_link_fullpath_hack()
{
    local link_target=$(ls -l "$1" | sed 's/^.* -> //');
    echo $(dirname "$1")/$link_target
}

##------------------------------------------------------------------------------
_manpdf_open()
{
    local PATH_TO_OPEN="$1";
    local CURR_OS="$(pw_os_get_simple_name)";

    ##
    ## @todo(stdmatt): The ps2pdf isn't supported by Msys2
    ## so we gonna have find and alternative later...
    case "$CURR_OS" in
        "$(PW_OS_OSX)" )
            open "$PATH_TO_OPEN"
        ;;
        "$(PW_OS_GNU_LINUX)" )
            xdg-open "$PATH_TO_OPEN" > /dev/null 2>&1 &
        ;;
        "$(PW_OS_WSL)" )
            READER=$(echo "$MANPDF_READER");
            if [ -z "$READER" ]; then
                pw_log_fatal "No for wsl was found"                                      \
                    "Export the MANPDF_READER variable with the path for the pdf reader." \
                    "Aborting...";
            fi;

            ## If reader has the extension of ".exe" means that we are calling
            ## a native Windows program and we need to translate the path to it
            ## Otherwise we are using native GNU/Linux and no translation is
            ## needed.
            if [ -n "$(pw_string_ends_with "$READER" ".exe")" ]; then
                "$READER" "$(wslpath -am $PATH_TO_OPEN)";
            else
                "$READER" "$PATH_TO_OPEN" > /dev/null 2>1 &
            fi;
        ;;
    esac
}

##----------------------------------------------------------------------------##
## Public Functions                                                           ##
##----------------------------------------------------------------------------##
##------------------------------------------------------------------------------
manpdf_help()
{
     cat <<'EOFFOE'
Usage:
  manpdf [--help] [--version] [--sync] [--show-config]
  manpdf [--setup] [--setup <output-path> <git-url>]
  manpdf [section] page

Options:
    * --help                          - Show this screen.
    * --version                       - Show copyright and version information.
    * --sync                          - Synchronize the saved manual pages.
    * --setup                         - Setup the program settings - Interactive mode.
    * --setup <output-path> <git-url> - Setup the program settings.
    * --show-config                   - Shows the current configuration.

Notes:
  Options marked with * are exclusive i.e. they'll run and exit without
  considering the other options.
EOFFOE
}

##------------------------------------------------------------------------------
manpdf_version()
{
    cat <<EOFFOE
$PROGRAM_NAME - $PROGRAM_VERSION - stdmatt <stdmatt@pixelwizards.io>
Copyright (c) $COPYRIGHT_YEARS - stdmatt
This is a free software (GPLv3) - Share/Hack it
Check http://stdmatt.com for more :)
EOFFOE
}

##------------------------------------------------------------------------------
_mandpf_write_settings()
{
    local output_path="$1";
    local git_url="$2";

    mkdir -p "$output_path";
    if [ $? != 0 ]; then
        pw_log_fatal "Error while creating directory: ($output_path)";
    fi;

    rm -rf "$RC_PATH";

    echo "output-path = $output_path";
    echo "gitrepo     = $git_url";

    echo "output-path = $output_path" >> "$RC_PATH";
    echo "gitrepo     = $git_url"     >> "$RC_PATH";

    echo "Settings written to:";
    echo "   $RC_PATH";
}

##------------------------------------------------------------------------------
manpdf_setup()
{
    ##
    ## Got the arguments from command line
    ##   So we don't need to run the prompts...
    local args_count=$(pw_count_words "$@");
    if [ "$args_count" == "2" ]; then
        _mandpf_write_settings "$1" "$2";
        return;
    fi;

    ##
    ## Output path.
    local output_path="";
    echo "output directory (let empty to use the default):";
    read output_path;

    if [ -z "$output_path" ]; then
        output_path="$DEFAULT_OUTPUT_PATH";
    fi;

    ##
    ## Git url.
    local git_url;
    echo "git repository url to enable sync:";
    read git_url;

    ##
    ## Confirm.
    echo "output directory: ($output_path)";
    echo "git url         : ($git_url)";

    local yes_no="";
    echo "Use this config? [yes/NO]";
    read yes_no;

    ##
    ## Save the settings.
    yes_no=$(pw_to_lower "$yes_no");
    if [ "$yes_no" == "yes" ]; then
        _mandpf_write_settings "$output_path" "$git_url";
    else
        echo "Ignoring all changes...";
    fi;
}

##------------------------------------------------------------------------------
manpdf_show_config()
{
    cat "$RC_PATH";
}


##------------------------------------------------------------------------------
manpdf_open()
{
    ##
    ## Assume that we're passing the full format.
    local section=$1;
    local page=$2;

    ##
    ## Since we can pass only the page we need adjust the vars here.
    if [ -z "$page" ]; then
        page=$section;
        section="";
    fi;

    ##
    ## Check if we have parameters...
    test -z "$section" && test -z "$page" && \
        pw_log_fatal "Missing manual section...";

    ##
    ## Find the path of manual
    local man_path=$(man -w $section $page);
    test -L "$man_path" && \
        man_path=$(_manpdf_get_link_fullpath_hack "$man_path");

    ##
    ## Check if we have a valid manual.
    if [ -z "$man_path" ]; then
        pw_log_fatal "There's no manual for ($page) in section ($section)";
    fi;

    ##
    ## Get the section if it isn't specified.
    local man_info=$(basename "$man_path" ".gz");
    real_path=$(   echo $man_info | cut -d"." -f1);
    real_section=$(echo $man_info | cut -d"." -f2);

    ##--------------------------------------------------------------------------
    ## The man pages are separated in:
    ##   1   Executable programs or shell commands
    ##   2   System calls (functions provided by the kernel)
    ##   3   Library calls (functions within program libraries)
    ##   4   Special files (usually found in /dev)
    ##   5   File formats and conventions eg /etc/passwd
    ##   6   Games
    ##   7   Miscellaneous (including macro packages and conventions), e.g. man(7), groff(7)
    ##   8   System administration commands (usually only for root)
    ##   9   Kernel routines [Non standard]
    ##
    ## We gonna separate our man pages into 2 sections only:
    ##   System - For everything that aren't "callable" from C.
    ##   C      - For everything that are "callable" from C.
    ##
    local output_section="";
    case $real_section in
        1 | 4 | 5 | 7 | 8 | 9 ) output_section="system"; ;;
        2 | 3                 ) output_section="C";      ;;
    esac

    ##
    ## If we don't have an defined output path we can't continue...
    test ! -d "$(_manpdf_get_output_path)" &&  \
        pw_log_fatal                           \
            "There's no output path set..."    \
            "Please run manpdf --setup to configure.";
    ##
    ## Where we gonna save the manual.
    local curr_os="$(pw_os_get_simple_name)";

    local filename="${curr_os}_${real_path}.pdf";
    local output_dir="$(_manpdf_get_output_path)/$output_section";
    local output_path="$output_dir/$filename";

    echo "section        : $(pw_FM $real_section)";
    echo "page           : $(pw_FM $real_path)";
    echo "Man Path       : $(pw_FM $man_path)";
    echo "Output section : $(pw_FM $output_section)";
    echo "Output Path    : $(pw_FM $output_path)";

    ##
    ## Create the directory if needed.
    mkdir -vp $output_dir;

    ##
    ## Manual page doesn't exists yet...
    if [ ! -e $output_path ]; then
        echo "Manual doesn't exists yet - Creating now..."
        ## Redirect stderr          Quiet and from stdin.
        man -t $1 $2 2> /dev/null | ps2pdf -dQUIET - $output_path;
    fi;

    _manpdf_open "$output_path";
}

##------------------------------------------------------------------------------
pypdf_open()
{
    echo "$@";
    local python_ver="python";
    if [ "$1" == "--python3" ]; then
        python_ver="python3";
    fi;

    ##
    ## Check if we have parameters...
    local item_to_search="$2";
    if [ -z "$item_to_search" ]; then
        pw_log_fatal "Missing pydoc item to search...";
    fi;

    local PYDOC_TEMP_FILE="/var/tmp/pypdf_temp_file.txt";

    ##
    ## Find the path of manual
    $python_ver -m pydoc "$item_to_search" > "$PYDOC_TEMP_FILE";

    ##
    ## Check if we got a valid manual page.
    local not_found=$(grep "no Python documentation found for '$item_to_search'" "$PYDOC_TEMP_FILE");
    if [ -n "$not_found" ]; then
        pw_log_fatal "$not_found";
    fi;

    ##
    ## If we don't have an defined output path we can't continue...
    test ! -d $(_manpdf_get_output_path) &&  \
        pw_log_fatal                         \
            "There's no output path set..."  \
            "Please run manpdf --setup to configure.";

    ##
    ## Where we gonna save the manual.
    local curr_os="$(pw_os_get_simple_name)";

    local filename="${curr_os}_${item_to_search}.pdf";

    local output_dir="$(_manpdf_get_output_path)/${python_ver}";
    local output_path="$output_dir/$filename";

    echo "Output Path : $(pw_FM $output_path)";

    ##
    ## Create the directory if needed.
    mkdir -vp $output_dir;

    ##
    ## Manual page doesn't exists yet...
    if [ ! -e $output_path ]; then
        echo "Manual doesn't exists yet - Creating now..."
        echo "import pdfkit; pdfkit.from_url(\"${PYDOC_TEMP_FILE}\", \"${output_path}\")" | python3 - > /dev/null
    fi;

    _manpdf_open "$output_path";
}

##------------------------------------------------------------------------------
manpdf_sync()
{
    ##
    ## Get the repo url.
    local url=$(_manpdf_get_pages_repo_url);
    if [ -z "$url" ]; then
        pw_log_fatal                                               \
            "Missing git repository url, can't sync - Aborting..." \
            "Run manpdf --setup to configure the sync options.";
    fi;

    ##
    ## Get the output path.
    local output_path=$(_manpdf_get_output_path);
    if [ -z "$url" ]; then
        pw_log_fatal                                                       \
            "Missing the pages output directory, can't sync - Aborting..." \
            "Run manpdf --setup to configure the sync options.";
    fi;

    ##
    ## Create the output directory if it not exists yet.
    if [ ! -d "$output_path" ]; then
        git clone "$url" "$output_path";
        if [ $? != 0 ]; then
            echo "";  ## Just to add a space line...
            pw_log_fatal                                                              \
                "Invalid git repository url or output path, can't sync - Aborting..." \
                "Run manpdf --setup to configure the sync options";
        fi;
    fi;


    ##
    ## Go to the output directory, it's much more easier to perform
    ## the git operations on a "local" directory.
    cd "$output_path";
    git init > /dev/null 2>&1; ## @notice(stdmatt): It's safe to rerun init....

    local remote_url=$(git remote -v | head -1 | expand -t1 | cut -d" " -f2);

    ##
    ## Check if we have remote..
    if [ -z "$remote_url" ]; then
        echo "[manpdf] Git remote url isn't present on the output directory ($output_path).";
        echo "   Adding ($url) as remote...";
        git remote add origin "$url";
    ##
    ## Check if the previous remote is the same - We can't perform anything
    ## if they're different since it might lost users data.
    elif [ "$remote_url" != "$url" ]; then
        pw_log_fatal                                                      \
            "Git repository remote url and the .rc file url don't match." \
            ""                                                            \
            "Repository url : $remote_url"                                \
            "rc file url    : $url"                                       \
            ""                                                            \
            "We can't peform anything without risking to lose data."      \
            "Check the repository remotes manually!"
    fi;

    ##
    ## Commit everything that we have on local.
    ## @todo(stdmatt) Would be nice to log the pages modified???
    git add .
    git commit -m "[manpdf_sync] $(date -Ru)"; ## @todo -Ru is BSD specific???

    ##
    ## Accept everything from remote.
    git pull --commit --no-edit origin master;

    ##
    ## Sync...
    git push origin master;
}


##----------------------------------------------------------------------------##
## Script                                                                     ##
##----------------------------------------------------------------------------##
if [ -n "$(pw_getopt_exists --help $@)" ]; then
    manpdf_help;
elif [ -n "$(pw_getopt_exists --version $@)" ]; then
    manpdf_version;
elif [ -n "$(pw_getopt_exists --setup $@)" ]; then
    ARGS="$@";
    ARGS=$(pw_string_replace "$ARGS" "--setup" "");
    manpdf_setup $ARGS;
elif [ -n "$(pw_getopt_exists --show-config $@)" ]; then
    manpdf_show_config;
elif [ -n "$(pw_getopt_exists --sync $@)" ]; then
    manpdf_sync;
else
    program_name=$(basename "$0");
    if [ "$program_name" == "manpdf" ]; then
        manpdf_open "$@";
    elif [ "$program_name" == "pypdf" ]; then
        pypdf_open "$@";
    fi;
fi;
